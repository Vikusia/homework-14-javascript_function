(function ex2(){
    let count = 10;
    let min = 0;
    let max = 15;

    function array(count, min, max) {
        let mas = [];

        for (let i = 0; i < count; i++) {
            mas.push(getRandomIntInclusive(min, max))
        }

        return mas
    }

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
    }

    console.log(array(count, min, max));
    console.log('--------------------------');
})();

