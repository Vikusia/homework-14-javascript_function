(function ex3() {
    let mas1 = [1, 5, 6, 10, 15, 40, 6];
    let mas2 = [1, 23, 6, 10, 12, 7, 6, 11, 13];

    function arrayMerge(mas1, mas2, callback) {
        let masResult = [];
        for (let i of mas1) {
            if (callback(i)) {
                masResult.push(i)
            }
        }
        for (let i of mas2) {
            if (callback(i)) {
                masResult.push(i)
            }
        }
        return masResult;
    }

    function check(i) {
        return i >= 5 && i <= 10

    }

    function check2(i) {
        return i >= 0 && i <= 6

    }

    console.log(mas1);
    console.log(mas2);

    console.log(arrayMerge(mas1, mas2, check));
    console.log(arrayMerge(mas1, mas2, check2));
    console.log(arrayMerge(mas1, mas2, (i) => i <= 2 || i === 5));
})();